package com.ts.Product;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dao.ProductDAO;
import com.model.Product;

@RestController
public class productController {
	
	@RequestMapping("getProductById/{ID}")
	public Product getProductById (@PathVariable("ID") int id) {
	return productDAO.getProductById(id);
	}
	
	@Autowired
	ProductDAO productDAO;
	
	@RequestMapping("/getAllProducts")
	public List<Product> getAllProducts(){
		return productDAO.getAllProduct();
	}
	
	@RequestMapping("/showProduct")
	public Product showProduct() {
		Product po = new Product(121,"mohan",234.0);
		return po;
	}
	
	@RequestMapping("/getProducts")
	public List<Product> showProducts(){
		List <Product> ProductList = new ArrayList<>();
		
		ProductList.add(new Product(1, "realme", 299999));
		ProductList.add(new Product(2, "Apple", 499999));
		ProductList.add(new Product(3, "MI", 799999));
		return ProductList;
		
	}
	@PostMapping("registerProduct")
	public Product registerProduct(@RequestBody Product product) {
		return productDAO.registerProduct(product);
	}
	

}
