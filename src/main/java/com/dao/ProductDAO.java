package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Product;

@Service
public class ProductDAO {

	@Autowired
	ProductRepository productRepository;

	public List<Product> getAllProduct(){

		return productRepository.findAll();
	}
	public Product getProductById(int id) {
		Product prod =new Product(1,"Sample Product",99999.99);
		return productRepository.findById(id).orElse(prod);
	}
	public Product registerProduct(Product product) {
		return productRepository.save(product);
	}

	public Product getProductByName(String name) {
		return productRepository.findByName(name);
	}
	public Product updateProduct(Product product) {
		return productRepository.save(product);
	}
}