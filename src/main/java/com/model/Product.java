package com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Product {
	// Dependency Injection
	@Id
	@GeneratedValue
	private int prodId;
	private String name;
	private double price;

	public Product() {

	}
	public Product(int prodId, String prodName, double price) {
		super();
		this.prodId=prodId;
		this.name=prodName;
		this.price=price;
	}
	public int getProdId() {
		return prodId;
	}
	public void setProdId(int prodId) {
		this.prodId = prodId;
	}
	public String getProdName() {
		return name;
	}
	public void setProdName(String prodName) {
		this.name = prodName;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	
	
}